//
//  ViewController.swift
//  DynamicsDemo
//
//  Created by James Cash on 08-06-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var animator: UIDynamicAnimator!
    var alertView: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()

        animator = UIDynamicAnimator(referenceView: view)
    }

    @IBAction func showAlert(_ sender: Any) {
        if alertView == nil {
            createAlertView()
        }

        let snap = UISnapBehavior(item: alertView!, snapTo: CGPoint(x: 200, y: 350))
        animator.addBehavior(snap)
    }

    func dismissAlert(_ sender: Any) {
        animator.removeAllBehaviors()

        let gravity = UIGravityBehavior(items: [alertView!])
        //gravity.angle = CGFloat.pi * 3 / 4
        gravity.magnitude = 10
        animator.addBehavior(gravity)

        let spin = UIDynamicItemBehavior(items: [alertView!])
        spin.addAngularVelocity(CGFloat.pi * 2, for: alertView!)
        animator.addBehavior(spin)

        UIView.animate(withDuration: 1.5, animations: { 
            self.alertView!.layer.opacity = 0
        }) { (success) in
            self.animator.removeAllBehaviors()
            self.alertView?.removeFromSuperview()
            self.alertView = nil
        }
    }

    func createAlertView() {
        let alert = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 150))
        alert.backgroundColor = UIColor.red
        alert.layer.cornerRadius = 10
        alert.layer.shadowOpacity = 0.7
        alert.layer.shadowColor = UIColor.black.cgColor
        alert.layer.shadowRadius = 5
        alert.layer.shadowOffset = CGSize(width: 5, height: 10)

        view.addSubview(alert)

        let btn = UIButton(type: .system)
        btn.frame = CGRect(x: (alert.frame.width - 20)/2,
                           y: 0, width: 20, height: 20)
        btn.setTitle("X", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.addTarget(self,
                      action: #selector(ViewController.dismissAlert(_:)),
                      for: .touchUpInside)

        alert.addSubview(btn)

        alertView = alert
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

